# Cucumber_Selenium

Setting Up the environment

- Download and install IntelliJ

- Clone the project to a local folder

- Open the project in intelliJ

- Right hand top corner ensure to allow intellij to install all neccecary plugins and dependencies  (It is an popup)

- Ensure Node.Js v5.6 is installed on your system

- For this versions to work, you need chrome installed Version 65.0.3325.181 (Official Build) (64-bit)

- Go to file, settings, plugins and install the following Cucumber for java and Gherkin.

- In both Projects you need to change the System.setProperty("webdriver.chrome.driver","{DirectoryPath to the webdriver for chrome}", same must be done inside wrong_data.java

The Form_Validation_Test are using an csv file to dynamically test the form validation. There are 2 scenario's where one works with propper data and other one not. In this example you can see that the one test pass,
but should fail, which means the validation are poorly done by the developer (which was me :( )

- In the validations.java file, you need to change    CSVReader read = new CSVReader(new FileReader("C:{Your Own Path}\\file.csv"));
- same must be done inside wrong_data.java  CSVReader read = new CSVReader(new FileReader("C:\\{Your Own Path}\\wrong_data.csv"));

- Finally go to the .feature file, right click on the feature you would like to execute and choose run scenario.