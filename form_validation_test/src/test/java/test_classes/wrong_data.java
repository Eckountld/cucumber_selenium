package test_classes;

import com.opencsv.CSVReader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileReader;

public class wrong_data {
    ChromeDriver driver;
    WebElement element;

    @When("^I open the rsvp page$")
    public void i_open_the_rsvp_page() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Then("^Enter all the types of emails with no name$")
    public void enter_all_the_types_of_emails_with_no_name() throws Throwable {

        CSVReader read = new CSVReader(new FileReader("C:\\repositories\\cucumber_selenium\\form_validation\\wrong_data.csv"));
        String[] cell;
        while((cell = read.readNext())!=null)
        {
            driver.get("http:/rsvp.eohdt.co.za/rsvp.html");
            for (int i = 0; i < 1; i++)
            {
                String FullName = cell[i];
                String Email = cell[i+1];

                driver.findElement(By.name("fullname")).sendKeys(FullName);
                driver.findElement(By.name("email")).sendKeys(Email);
                driver.findElement(By.className("btn")).submit();
            }

        }
    }

    @Then("^I close the browser$")
    public void i_close_the_browser() throws Throwable {

        System.out.println("If this test Passed, we know that the Validation is not working correctly");
            driver.close();
    }
}
