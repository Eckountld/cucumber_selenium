package test_classes;

import com.opencsv.CSVReader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.FileReader;


public class validations {
      ChromeDriver driver;
      WebElement element;
    @When("^I open the rsvp website$")
    public void i_open_the_rsvp_website() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();

    }

    @Then("^Enter all my crecedentials$")
    public void enter_all_my_crecedentials() throws Throwable {

        CSVReader read = new CSVReader(new FileReader("C:\\repositories\\cucumber_selenium\\form_validation\\file.csv"));
        String[] cell;
        while((cell = read.readNext())!=null)
        {
            driver.get("http://rsvp.eohdt.co.za/rsvp.html");
            for (int i = 0; i < 1; i++)
            {
                String FullName = cell[i];
                String Email = cell[i+1];
                driver.findElement(By.name("fullname")).sendKeys(FullName);
                driver.findElement(By.name("email")).sendKeys(Email);
                driver.findElement(By.className("btn")).submit();
            }

        }
    }

    @Then("^a stupid spam message will popup$")
    public void a_stupid_spam_message_will_popup() throws Throwable {

       driver.close();
    }
}
